# Описание API для управления пользователями и их зарплатами

## Описание проекта

Проект представляет собой API для проверки пользователем зарплаты и даты повышения. Пользователи могут регистрироваться, входить в систему и просматривать информацию о своей зарплате и следующем повышении.

## Эндпоинты API

### Регистрация пользователя

POST /users/

#### Пример запроса

```json
{
  "username": "string",
  "password": "string"
}
```
#### Пример успешного ответа
```json
{
  "username": "string",
  "password": "string",
  "salary": 0,
  "next_promotion_date": "string"
}
```

### Вход пользователя

POST /token/

#### Пример запроса

```json
{
  "username": "string",
  "password": "string"
}
```
#### Пример успешного ответа

```json
{
  "access_token": "string"
}
```
### Получение информации о зарплате пользователя

GET /salary/

#### Пример запроса

```
GET /salary/?token=example_token
```

#### Пример успешного ответа

```json
{
  "salary": 50000,
  "promotion_date": "2024-04-01"
}
```

## Описание моделей

#### User
Модель пользователя.

```json
{
  "username": "example_user",
  "password": "hashed_password"
}
```

#### Token
Модель токена доступа.

```json
{
  "access_token": "example_token",
}
```
#### UserInDB

Модель пользователя в базе данных.

```json
{
  "salary": "example_salary",
  "promotion_date": "example_promotion_date"
}
```

## Запуск проекта
#### Установка виртуального окружения
Создайте и активируйте вируальное окружение:
```
python3 -m venv venv
source venv/scripts/activate
```
Установите poetry и зависимости из файла pyproject.toml:
```
pip install poetry
poetry install
```

Запустите проект:
```
uvicorn main:app
```
Тестируйте эндпоинты в Swagger по ссылке:
```
http://127.0.0.1:8000/docs
```

#### Загрузка проекта через Docker
соберите образ командой:
```
docker build -t ваше_название .
```
запустите контейнер:
```
docker run -p 8000:8000 fastapi_salary
```
Документация с эндпоинтами будет доступна по ссылке:
```
http://localhost:8000/docs
```

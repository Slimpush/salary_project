import jwt
import pytest

from fastapi.testclient import TestClient
from main import ALGORITHM, SECRET_KEY, User, app


@pytest.fixture
def client():
    with TestClient(app) as client:
        yield client


def test_create_user(client):
    user = User(username="Jane", password="password")
    response = client.post("/users/", json=user.dict())
    assert response.status_code == 201
    created_user = response.json()
    assert created_user["username"] == "Jane"
    assert created_user["salary"] == 0
    assert created_user["next_promotion_date"] == "N/A"


def test_login(client):
    new_user_password = "new_password"
    user = User(username="John", password=new_user_password)
    client.post("/users/", json=user.dict())
    response = client.post("/token/", json=user.dict())
    assert response.status_code == 200
    token_data = response.json()
    assert "access_token" in token_data
    access_token = token_data["access_token"]
    payload = jwt.decode(access_token, SECRET_KEY, algorithms=[ALGORITHM])
    assert payload["sub"] == "John"


def test_read_salary(client):
    new_user_password = "new_password"
    user = User(username="John", password=new_user_password)
    client = TestClient(app)
    response = client.post("/token/", json={"username": user.username,
                                            "password": new_user_password})
    assert response.status_code == 200
    token_data = response.json()
    assert token_data["access_token"] is not None
    access_token = token_data["access_token"]
    payload = jwt.decode(access_token, SECRET_KEY, algorithms=[ALGORITHM])
    assert payload["sub"] == "John"


def test_invalid_username(client):
    user = User(username="Invalid", password="password")
    response = client.post("/token/", json=user.dict())
    assert response.status_code == 401
    assert response.json()["detail"] == "Invalid username or password"


def test_invalid_password(client):
    user = User(username="John", password="invalid")
    response = client.post("/token/", json=user.dict())
    assert response.status_code == 401
    assert response.json()["detail"] == "Invalid username or password"

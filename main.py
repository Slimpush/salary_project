import secrets
from datetime import datetime, timedelta, timezone
from typing import Dict, Union

import jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from fastapi import FastAPI, HTTPException, status

app = FastAPI()

SECRET_KEY: str = secrets.token_hex(32)
ALGORITHM: str = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES: int = 20


user_database = {
    "user1": {
        "username": "John",
        "password":
        "hashed_password",
        "salary": 50000,
        "promotion_date": "2024-04-01"
    }
}


class User(BaseModel):
    # Модель данных для пользователя
    username: str
    password: str


class Token(BaseModel):
    # Модель данных для токена
    access_token: str


class UserInDB(User):
    # Модель данных для пользователя в БД
    salary: int
    next_promotion_date: str


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    # Проверка пароля
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    # Хеширование пароля
    return pwd_context.hash(password)


def get_user(username: str) -> Union[UserInDB, None]:
    # Получение пользователя из базы данных
    if username in user_database:
        user_dict = user_database[username]
        return UserInDB(**user_dict)
    return None


def authenticate_user(username: str, password: str) -> Union[UserInDB, bool]:
    # Аутентификация пользователя
    user = get_user(username)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta) -> str:
    # Создание токена доступа
    to_encode = data.copy()
    expire = datetime.now(timezone.utc) + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


@app.post("/users/", response_model=UserInDB,
          status_code=status.HTTP_201_CREATED)
async def create_user(user: User) -> Dict[str, Union[str, int]]:
    # Создание нового пользователя
    if user.username in user_database:
        raise HTTPException(
            status_code=400,
            detail="User already exists",
        )
    hashed_password = get_password_hash(user.password)
    user_data = {
        "username": user.username,
        "password": hashed_password,
        "salary": 0,
        "next_promotion_date": "N/A",
    }
    user_database[user.username] = user_data
    return user_data


@app.post("/token/", response_model=Token)
async def login(user: User) -> Token:
    # Аутентификация пользователя и создание токена
    user = authenticate_user(user.username, user.password)
    if not user:
        raise HTTPException(status_code=401,
                            detail="Invalid username or password",
                            )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        {"sub": user.username},
        access_token_expires,
        )
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/salary/", response_model=dict)
async def read_salary(token: str) -> dict:
    # Получение информации о зарплате и повышении пользователя
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        current_user = get_user(payload["sub"])
        if not current_user:
            raise HTTPException(status_code=401, detail="Invalid token")
        return {"salary": current_user.salary,
                "promotion_date": current_user.next_promotion_date,
                }
    except jwt.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Token has expired")
    except jwt.InvalidTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")
